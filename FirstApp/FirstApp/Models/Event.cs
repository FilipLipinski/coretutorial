﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FirstApp.Models
{
    public enum EventStatus
    {
        [Display(Name = "Przydzielony do etapu - planowany na nadchodzące lata")]
        PrzypisanyDoEtapu = 0,
        [Display(Name = "Wstępnie zaplanowany")]
        WstepnieZaplanowany = 1,
        [Display(Name = "Zaplanowany z detalami")]
        Zaplanowany = 2,
        [Display(Name = "Zrobione!")]
        Wykonany = 3
    }

    public class Event
    {
        public int EventId { get; set; }
        [Required]
        public string EventName { get; set; }
        [Required]
        public string MountainName { get; set; }
        [Required]
        public string MountainRange { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        [DataType(DataType.Date)]
        public DateTime DateOfCompletion { get; set; }
        public string PlannedDate { get; set; }
        public string Location { get; set; }
        public int DistanceToTravel { get; set; }
        [Required]
        public int MountainHeight { get; set; }
        public int EstimatedHeightToClimb { get; set; }
        //use in statistics
        public int NumberOfPeopleInvolved { get; set; }
        public int DistnceTraveledOnFootInMeters { get; set; }
        public int MountainsProminenceTravelled { get; set; }
        [Required]
        public int Status { get; set; }
        public string MapHtml { get; set; }
        public string Image { get; set; }

        [Required]
        public int StageId { get; set; }
        public Stage Stage { get; set; }

    }
}
