﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstApp.Models
{
    public class StageRepository: IStageRepository
    {
        private readonly AppDbContext _appDbContext;

        public StageRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<Stage> Stages => _appDbContext.Stages;

        public void AddStage(Stage stage)
        {
            _appDbContext.Stages.Add(stage);
            _appDbContext.SaveChanges();
        }

        public void EditStage(Stage stage)
        {
            _appDbContext.Stages.Update(stage);
            _appDbContext.SaveChanges();
        }

        public void DeleteStage(Stage stage)
        {
            _appDbContext.Stages.Remove(stage);
            _appDbContext.SaveChanges();
        }

        public Stage GetStageById(int stageId) => _appDbContext.Stages.FirstOrDefault(s => s.StageId == stageId);
        public IQueryable<Event> GetStageEventsById(int stageId) => _appDbContext.Events.Where(s => s.StageId == stageId);
        public int GetStageEventsNumber(int stageId) => GetStageEventsById(stageId).Count();
    }
}
