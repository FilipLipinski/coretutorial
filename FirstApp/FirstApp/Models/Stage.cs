﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FirstApp.Models
{
    public class Stage
    {
        public int StageId { get; set; }
        [Required]
        public string StageName { get; set; }
        public string StageGoal { get; set; }

        public List<Event> Events { get; set; }

        public Stage() { }

        public Stage(Stage stage)
        {
            StageId = stage.StageId;
            StageName = stage.StageName;
            StageGoal = stage.StageGoal;
        }

    }
}
