﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstApp.ViewModels;

namespace FirstApp.Models
{
    public interface IEventRepository
    {
        IEnumerable<Event> Events { get; }

        Event GetEventById(int eventId);

        IQueryable<Event> GetCompletedEvents();
        void AddEvent(Event ev);
        void EditEvent(Event ev);
        void RemoveEvent(Event ev);
    }
}
