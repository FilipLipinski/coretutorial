﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstApp.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace FirstApp.Models
{
    public class EventRepository: IEventRepository
    {
        private readonly AppDbContext _appDbContext;

        public EventRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<Event> Events => _appDbContext.Events.OrderBy(e => e.MountainHeight);

        public Event GetEventById(int eventId) => _appDbContext.Events.FirstOrDefault(p => p.EventId == eventId);

        public IQueryable<Event> GetCompletedEvents() => _appDbContext.Events.Where(ev => ev.Status == 3);

        public void AddEvent(Event ev)
        {
            _appDbContext.Events.Add(ev);
            _appDbContext.SaveChanges();
        }

        public void EditEvent(Event ev)
        {
            _appDbContext.Events.Update(ev);
            _appDbContext.SaveChanges();
        }

        public void RemoveEvent(Event ev)
        {
            _appDbContext.Events.Remove(ev);
            _appDbContext.SaveChanges();
        }
    }
}
