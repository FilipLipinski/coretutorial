﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace FirstApp.Models
{
    public class Feedback
    {
        [BindNever]
        public int FeedbackId { get; set; }

        [Required(ErrorMessage = "To pole jest wymagane")]
        [StringLength(50, MinimumLength = 5)]
        [Display(Name = "Imię i nazwisko")]
        public string Name { get; set; }

        [Required(ErrorMessage = "To pole jest wymagane")]
        [StringLength(50, MinimumLength = 5)]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", 
            ErrorMessage = "Podany mail ma niepoprawny format")]
        
        public string Email { get; set; }

        [Required(ErrorMessage = "To pole jest wymagane")]
        [StringLength(3000)]
        [Display(Name = "Wiadomość")]
        public string Message { get; set; }

        [Display(Name = "Proszę o kontakt")]
        public bool ContactMe { get; set; }
    }
}
