﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstApp.Models
{
    public interface IStageRepository
    {
        IEnumerable<Stage> Stages { get; }

        void AddStage(Stage stage);
        void EditStage(Stage stage);
        void DeleteStage(Stage stage);

        Stage GetStageById(int stageId);
        IQueryable<Event> GetStageEventsById(int stageId);
        int GetStageEventsNumber(int stageId);
    }
}
