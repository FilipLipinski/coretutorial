﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace FirstApp.Models
{
    public static class DbInitializer
    {
        public static void Seed(AppDbContext context)
        {

            if (!context.Categories.Any())
            {
                context.AddRange(
                    new Category {CategoryName = "Bardzo mała odległość", Description = "Do 100 km od Wrocławia"},
                    new Category {CategoryName = "Mała odległość", Description = "100-200 km od Wrocławia"},
                    new Category {CategoryName = "Średnia odległość", Description = "200-350 km od Wrocławia"},
                    new Category {CategoryName = "Duża odległość", Description = "350+ km od Wrocławia"}
                );
            }

            if (!context.Events.Any())
            {
                context.AddRange(
                    new Event { EventName = "", MountainName = "Łysica", MountainRange = "Góry Świętokrzyskie", DistanceToTravel = 340, MountainHeight = 612, StageId = 1},
                    new Event { EventName = "", MountainName = "Kłodzka Góra", MountainRange = "Góry Bardzkie", DistanceToTravel = 90, MountainHeight = 765, StageId = 1},
                    new Event { EventName = "", MountainName = "Biskupia Kopa", MountainRange = "Góry Opawskie", DistanceToTravel = 120, MountainHeight = 889, StageId = 1 },
                    new Event { EventName = "", MountainName = "Skalnik", MountainRange = "Rudawy Janowickie", DistanceToTravel = 110, MountainHeight = 945, StageId = 1 },
                    new Event { EventName = "", MountainName = "Kowadło", MountainRange = "Góry Złote", DistanceToTravel = 130, MountainHeight = 989, StageId = 1 },
                    new Event { EventName = "", MountainName = "Rudawiec", MountainRange = "Góry Bialskie", DistanceToTravel = 140, MountainHeight = 1112, StageId = 1 },
                    new Event { EventName = "", MountainName = "Mogielnica", MountainRange = "Beskid Wyspowy", DistanceToTravel = 340, MountainHeight = 1170, StageId = 1 },
                    new Event { EventName = "", MountainName = "Turbacz", MountainRange = "Gorce", DistanceToTravel = 1310, MountainHeight = 360, StageId = 1 },
                    new Event { EventName = "", MountainName = "Rysy", MountainRange = "Tatry", DistanceToTravel = 2499, MountainHeight = 370, StageId = 1 }
                    );
            }

            if (!context.Stages.Any())
            {
                context.AddRange(
                    new Stage { StageName = "Etap I - Rok 2019"},
                    new Stage { StageName = "Etap II - Rok 2020"},
                    new Stage { StageName = "Etap III - Rok 2021"});
            }

            if (!context.UserRoles.Any())
            {
                var role = new IdentityRole("Admin");
                context.Roles.AddAsync(role);
                role = new IdentityRole("User");
                context.Roles.AddAsync(role);
            }

            context.SaveChanges();
        }
    }
}
