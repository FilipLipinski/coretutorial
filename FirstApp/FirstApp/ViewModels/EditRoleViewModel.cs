﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstApp.Auth;
using Microsoft.AspNetCore.Identity;

namespace FirstApp.ViewModels
{
    public class EditRoleViewModel : AddRoleViewModel
    {
        public List<ApplicationUser> Users { get; set; }
    }
}
