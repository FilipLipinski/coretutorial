﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace FirstApp.ViewModels
{
    public class AddRoleToUserViewModel
    {
        public AddRoleToUserViewModel()
        {
            Roles = new List<IdentityRole>();
        }

        public string UserId { get; set; }
        public string RoleId { get; set; }
        public List<IdentityRole> Roles { get; set; }
    }
}
