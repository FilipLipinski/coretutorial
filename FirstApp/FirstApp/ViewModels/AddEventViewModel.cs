﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;

namespace FirstApp.ViewModels
{
    public class AddEventViewModel : Event
    {
        public AddEventViewModel()
        {
            StagesList = new List<Stage>();
        }

        public AddEventViewModel(Event ev)
        {
            EventId = ev.EventId;
            EventName = ev.EventName;
            MountainName = ev.MountainName;
            MountainRange = ev.MountainRange;
            ShortDescription = ev.ShortDescription;
            LongDescription = ev.LongDescription;
            DateOfCompletion = ev.DateOfCompletion;
            PlannedDate = ev.PlannedDate;
            Location = ev.Location;
            DistanceToTravel = ev.DistanceToTravel;
            MountainHeight = ev.MountainHeight;
            EstimatedHeightToClimb = ev.EstimatedHeightToClimb;
            NumberOfPeopleInvolved = ev.NumberOfPeopleInvolved;
            DistnceTraveledOnFootInMeters = ev.DistnceTraveledOnFootInMeters;
            MountainsProminenceTravelled = ev.MountainsProminenceTravelled;
            MapHtml = ev.MapHtml;
            StageId = ev.StageId;
            Stage = ev.Stage;
            Status = ev.Status;
            Image = ev.Image;
            StagesList = new List<Stage>();
        }

        public List<Stage> StagesList { get; set; }
    }
}
