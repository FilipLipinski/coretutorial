﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstApp.Models;

namespace FirstApp.ViewModels
{
    public class EventsListViewModel
    {
        public List<Event> Events { get; set; }
    }
}
