﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstApp.Models;

namespace FirstApp.ViewModels
{
    public class StagesListViewModel
    {
        public List<Stage> Stages { get; set; }
        public List<int> NumberOfEventsPerStage { get; set; }
        public int StagesNumber { get; set; }
        public int EventsNumber { get; set; }
    }
}
