﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FirstApp.Migrations
{
    public partial class relationsStageEventAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Events_Stages_StageId",
                table: "Events");

            migrationBuilder.AlterColumn<int>(
                name: "StageId",
                table: "Events",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Events_Stages_StageId",
                table: "Events",
                column: "StageId",
                principalTable: "Stages",
                principalColumn: "StageId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Events_Stages_StageId",
                table: "Events");

            migrationBuilder.AlterColumn<int>(
                name: "StageId",
                table: "Events",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Events_Stages_StageId",
                table: "Events",
                column: "StageId",
                principalTable: "Stages",
                principalColumn: "StageId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
