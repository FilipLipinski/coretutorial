﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FirstApp.Migrations
{
    public partial class EventDatesExpanded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Date",
                table: "Events",
                newName: "DateOfCompletion");

            migrationBuilder.AddColumn<string>(
                name: "PlannedDate",
                table: "Events",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PlannedDate",
                table: "Events");

            migrationBuilder.RenameColumn(
                name: "DateOfCompletion",
                table: "Events",
                newName: "Date");
        }
    }
}
