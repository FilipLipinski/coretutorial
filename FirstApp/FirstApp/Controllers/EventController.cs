﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FirstApp.Models;
using FirstApp.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FirstApp.Controllers
{
    public class EventController : Controller
    {
        private readonly IEventRepository _eventRepository;
        private readonly IStageRepository _stageRepository;

        public EventController(IEventRepository iEventRepository, IStageRepository iStageRepository)
        {
            _eventRepository = iEventRepository;
            _stageRepository = iStageRepository;
        }

        public ViewResult List()
        {
            EventsListViewModel eventsListViewModel = new EventsListViewModel
            {
                Events = _eventRepository.Events.OrderBy(e => e.MountainHeight).ToList(),
            };

            return View(eventsListViewModel);
        }

        public IActionResult Details(int id)
        {
            var eevent = _eventRepository.GetEventById(id);
            if (eevent == null)
            return NotFound();
            return View(eevent);
        }

        public IActionResult EventManagement()
        {
            var events = _eventRepository.Events.OrderBy(o => o.MountainHeight);
            foreach (var ev in events)
            {
                ev.Stage = new Stage(_stageRepository.GetStageById(ev.StageId));
            }
            return View(events);
        }

        public IActionResult AddEvent()
        {
            var addEventViewModel = new AddEventViewModel();
            foreach (var stage in _stageRepository.Stages)
            {
                addEventViewModel.StagesList.Add(stage);
            }
            return View(addEventViewModel);
        }

        [HttpPost]
        public IActionResult AddEvent(AddEventViewModel addEventViewModel)
        {
            if (!ModelState.IsValid)
            {
                foreach (var stage in _stageRepository.Stages)
                {
                    addEventViewModel.StagesList.Add(stage);
                }
                return View(addEventViewModel);
            }
            Event ev = new Event
            {
                EventName = addEventViewModel.EventName,
                MountainName = addEventViewModel.MountainName,
                MountainRange = addEventViewModel.MountainRange,
                ShortDescription = addEventViewModel.ShortDescription,
                LongDescription = addEventViewModel.LongDescription,
                DateOfCompletion = addEventViewModel.DateOfCompletion,
                PlannedDate =  addEventViewModel.PlannedDate,
                Location = addEventViewModel.Location,
                DistanceToTravel = addEventViewModel.DistanceToTravel,
                MountainHeight = addEventViewModel.MountainHeight,
                EstimatedHeightToClimb = addEventViewModel.EstimatedHeightToClimb,
                NumberOfPeopleInvolved = addEventViewModel.NumberOfPeopleInvolved,
                DistnceTraveledOnFootInMeters = addEventViewModel.DistnceTraveledOnFootInMeters,
                MountainsProminenceTravelled = addEventViewModel.MountainsProminenceTravelled,
                Status = addEventViewModel.Status,
                MapHtml = addEventViewModel.MapHtml,
                Image =  addEventViewModel.Image,
                StageId = addEventViewModel.StageId
            };

            if (ModelState.IsValid)
            {
                _eventRepository.AddEvent(ev);
                return RedirectToAction("EventManagement");
            }

            return View(addEventViewModel);
        }

        public IActionResult EditEvent(int id)
        {
            Event ev =  _eventRepository.GetEventById(id);
            AddEventViewModel editEvent = new AddEventViewModel(ev);
            editEvent.Stage = _stageRepository.GetStageById(editEvent.StageId);
            foreach (var stage in _stageRepository.Stages)
            {
                editEvent.StagesList.Add(stage);
            }
            if (ev == null) return RedirectToAction("EventManagement", _eventRepository.Events);

            return View(editEvent);
        }

        [HttpPost]
        public IActionResult EditEvent(AddEventViewModel addEventViewModel)
        {
            if (!ModelState.IsValid)
            {
                foreach (var stage in _stageRepository.Stages)
                {
                    addEventViewModel.StagesList.Add(stage);
                }
                return View(addEventViewModel);
            }

            if (ModelState.IsValid)
            {
                _eventRepository.EditEvent(addEventViewModel);
                return RedirectToAction("EventManagement");
            }

            return View(addEventViewModel);
        }

        public IActionResult DeleteEvent(int id)
        {
            Event ev = _eventRepository.GetEventById(id);
            ev.Stage = _stageRepository.GetStageById(ev.StageId);
            return View(ev);
        }

        [HttpPost]
        public IActionResult DeleteEvent(Event eve)
        {
            Event ev =  _eventRepository.GetEventById(eve.EventId);
            if (ev != null)
            {
                _eventRepository.RemoveEvent(ev);
            }

            return View("EventManagement", _eventRepository.Events);
        }
    }
}
