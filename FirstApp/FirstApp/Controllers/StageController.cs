﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstApp.Models;
using FirstApp.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FirstApp.Controllers
{
    public class StageController : Controller
    {
        private readonly IStageRepository _iStageRepository;
        private readonly IEventRepository _iEventRepository;

        public StageController(IStageRepository stageRepository, IEventRepository eventRepository)
        {
            _iStageRepository = stageRepository;
            _iEventRepository = eventRepository;
        }

        // GET: Stage
        public ActionResult StageSummary(int id)
        {
            var stage = _iStageRepository.GetStageById(id);
            if (stage == null) return NotFound();
            stage.Events = _iStageRepository.GetStageEventsById(id).ToList();
            
            return View(stage);
        }

        public IActionResult StageDetails(int id)
        {
            var stage = _iStageRepository.GetStageById(id);
            if (stage == null) return NotFound();
            return View(stage);
        }

        public IActionResult StagesManagement()
        {
            var stages = _iStageRepository.Stages.OrderBy(s => s.StageName).ToList();
            return View(stages);
        }

        public IActionResult AddStage()
        {
            return View(new Stage());
        }

        [HttpPost]
        public IActionResult AddStage(Stage stage)
        {
            if (!ModelState.IsValid) return View(stage);
            if (ModelState.IsValid)
            {
                _iStageRepository.AddStage(stage);
                return RedirectToAction("StagesManagement", _iStageRepository.Stages);
            }

            return View(stage);
        }

        public IActionResult EditStage(int id)
        {
            var stage = _iStageRepository.GetStageById(id);
            if (stage == null) return NotFound();
            return View(stage);
        }

        [HttpPost]
        public IActionResult EditStage(Stage stage)
        {
            if (!ModelState.IsValid) return View(stage);
            if (ModelState.IsValid)
            {
                _iStageRepository.EditStage(stage);
                return RedirectToAction("StagesManagement", _iStageRepository);
            }
            return View(stage);
        }

        [HttpPost]
        public IActionResult DeleteStage(int id)
        {
            var stage = _iStageRepository.GetStageById(id);
            if (stage == null) return NotFound();
            _iStageRepository.DeleteStage(stage);

            return RedirectToAction("StagesManagement");
        }
    }
}