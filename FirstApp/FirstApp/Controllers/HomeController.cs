﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstApp.Models;
using FirstApp.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace FirstApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IStageRepository _stageRepository;
        private readonly IEventRepository _eventRepository;

        public HomeController(IStageRepository stageRepository, IEventRepository eventRepository)
        {
            _stageRepository = stageRepository;
            _eventRepository = eventRepository;
        }

        public IActionResult Index()
        {
            ViewBag.Plan = "Plan jest prosty - zdobyć najwyższy szczyt każdego pasma górskiego w Polsce!";
            ViewBag.Title = "Korona Gór Polski";
            ViewBag.Title2 = "czyli 28 wyzwań";
            ViewBag.DetailedPlan = "Całość podzielona jest na 3 etapy:";
            ViewBag.TotalEventsCount = _eventRepository.Events.Count();
            ViewBag.TotalStagesCount = _stageRepository.Stages.Count();
            ViewBag.TotalDistanceTraveled = CountTotalDistanceTravelled();
            ViewBag.TotalNumberOfPeopleInvolved = CountNuberOfPeopleInvolved();
            ViewBag.TotalDistanceTraveledOnFoot = CountTotalDistanceTraveledOnFoot();
            ViewBag.TotalMountainsProminence = CountTotalMountainsProminence();
            ViewBag.TotalCompletedEvents = GetCompletedEvents();

            StagesListViewModel stagesListViewModel = new StagesListViewModel
            {
                Stages = _stageRepository.Stages.OrderBy(c => c.StageId).ToList()
            };

            stagesListViewModel.NumberOfEventsPerStage = new List<int>();
            for (int i = 0; i < stagesListViewModel.Stages.Count; i++)
            {
                stagesListViewModel.NumberOfEventsPerStage.Add(_stageRepository.GetStageEventsNumber(stagesListViewModel.Stages[i].StageId));
            }
            return View(stagesListViewModel);
        }

        private int GetCompletedEvents()
        {
            return _eventRepository.GetCompletedEvents().Count();
        }

        private int CountTotalDistanceTravelled()
        {
            int totalDistanceTraveled = 0;
            foreach (var ev in _eventRepository.GetCompletedEvents())
            {
                totalDistanceTraveled += ev.DistanceToTravel;
            }
            return totalDistanceTraveled;
        }

        private int CountNuberOfPeopleInvolved()
        {
            int totalNumberOfPeopleInvolved = 0;
            foreach (var ev in _eventRepository.GetCompletedEvents())
            {
                totalNumberOfPeopleInvolved += ev.NumberOfPeopleInvolved;
            }
            return totalNumberOfPeopleInvolved;
        }

        private int CountTotalDistanceTraveledOnFoot()
        {
            int totalDistanceTravlledOnFoot = 0;
            foreach (var ev in _eventRepository.GetCompletedEvents())
            {
                totalDistanceTravlledOnFoot += ev.DistnceTraveledOnFootInMeters;
            }
            return totalDistanceTravlledOnFoot;
        }

        private int CountTotalMountainsProminence()
        {
            int totalMountainsProminence = 0;
            foreach (var ev in _eventRepository.GetCompletedEvents())
            {
                totalMountainsProminence += ev.MountainsProminenceTravelled;
            }
            return totalMountainsProminence;
        }
    }
}