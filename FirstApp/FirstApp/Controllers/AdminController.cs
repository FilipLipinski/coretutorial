﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstApp.Auth;
using FirstApp.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FirstApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public AdminController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public IActionResult UserManagement()
        {
            var users = _userManager.Users.OrderBy(o => o.Email);

            return View(users);
        }
        
        public IActionResult AddUser() => View();

        [HttpPost]
        public async Task<IActionResult> AddUser(AddUserViewModel addUserViewModel)
        {
            if ((!ModelState.IsValid)) return View(addUserViewModel);

            var user = new ApplicationUser()
            {
                UserName = addUserViewModel.UserName,
                Email = addUserViewModel.Email,
                BirthDate = addUserViewModel.BirthDate,
                City = addUserViewModel.City,
                Country = addUserViewModel.Country
            };

            IdentityResult result = await _userManager.CreateAsync(user, addUserViewModel.Password);

            if (result.Succeeded)
            {
                return RedirectToAction("UserManagement", _userManager.Users);
            }

            foreach (IdentityError error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }

            return View(addUserViewModel);
        }

        public async Task<IActionResult> EditUser(string id)
        {
            var user = await _userManager.FindByIdAsync(id);

            if (user == null) return RedirectToAction("UserManagement", _userManager.Users);

            return View(user);
        }

        [HttpPost]
        public async Task<IActionResult> EditUser(string id, string userName, string email, DateTime birthDate, string city, string country)
        {
            var user = await _userManager.FindByIdAsync(id);

            if (user != null)
            {
                user.Email = email;
                user.UserName = userName;
                user.BirthDate = birthDate;
                user.City = city;
                user.Country = country;
                var result = await _userManager.UpdateAsync(user);

                if (result.Succeeded) return RedirectToAction("UserManagement", _userManager.Users);

                ModelState.AddModelError("", "Update error...");

                return View(user);
            }

            return View(user);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteUser(string id)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(id);

            if (user != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
                if (result.Succeeded) return RedirectToAction("UserManagement");
                else ModelState.AddModelError("", "Something went wrong while deleting user");
            }
            else
            {
                ModelState.AddModelError("", "This user can't be found");
            }

            return View("UserManagement", _userManager.Users);
        }


        public IActionResult RoleManagement()
        {
            var roles = _roleManager.Roles;
            return View(roles);
        }

        public IActionResult AddRole() => View();

        [HttpPost]
        public async Task<IActionResult> AddRole(AddRoleViewModel addRoleViewModel)
        {
            if (!ModelState.IsValid) return View(addRoleViewModel);

            var role = new IdentityRole(addRoleViewModel.RoleName);

            IdentityResult result = await _roleManager.CreateAsync(role);

            if (result.Succeeded) return RedirectToAction("RoleManagement", _roleManager.Roles);

            return View(addRoleViewModel);
        }

        public async Task<IActionResult> EditRole(string id)
        {
            var role = await _roleManager.FindByIdAsync(id);

            if (role == null) return RedirectToAction("RoleManagement", _roleManager.Roles);
            
            var editRoleViewModel = new EditRoleViewModel
            {
                Id = role.Id,
                RoleName = role.Name,
                Users = new List<ApplicationUser>()
            };

            foreach (var user in _userManager.Users)
            {
                if (await _userManager.IsInRoleAsync(user, role.Name))
                    editRoleViewModel.Users.Add(user);
            }

            return View(editRoleViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> EditRole(EditRoleViewModel editRoleViewModel)
        {
            var role = await _roleManager.FindByIdAsync(editRoleViewModel.Id);

            if (role != null)
            {
                role.Name = editRoleViewModel.RoleName;

                var result = await _roleManager.UpdateAsync(role);

                if (result.Succeeded) return RedirectToAction("RoleManagement", _roleManager.Roles);

                ModelState.AddModelError("", "Role not updated, an error occured.");

                return View(editRoleViewModel);
            }

            return RedirectToAction("RoleManagement", _roleManager.Roles);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteRole(string id)
        {
            var role = await _roleManager.FindByIdAsync(id);

            if (role != null)
            {
                var result = await _roleManager.DeleteAsync(role);
                if (result.Succeeded) return RedirectToAction("RoleManagement", _roleManager.Roles);

                ModelState.AddModelError("", "Something went wrong, role was not deleted.");
            }

            return View(_roleManager.Roles);
        }

        public async Task<IActionResult> SetUserRole(string Id)
        {
            var user = await _userManager.FindByIdAsync(Id);

            if (user == null) return RedirectToAction("UserManagement", _userManager.Users);

            var addRoleToUserViewModel = new AddRoleToUserViewModel{ UserId = user.Id};

            

            foreach (IdentityRole role in _roleManager.Roles)
            {
                if (await _userManager.IsInRoleAsync(user, role.Name)) addRoleToUserViewModel.RoleId = role.Id;
                addRoleToUserViewModel.Roles.Add(role);
            }

            return View(addRoleToUserViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> SetUserRole(AddRoleToUserViewModel addRoleToUserViewModel)
        {
            var user = await _userManager.FindByIdAsync(addRoleToUserViewModel.UserId);
            var role = await _roleManager.FindByIdAsync(addRoleToUserViewModel.RoleId);

            bool isInRole = await _userManager.IsInRoleAsync(user, role.Name);
            if (isInRole) return RedirectToAction("UserManagement", _userManager.Users);

            await _userManager.RemoveFromRolesAsync(user, await _userManager.GetRolesAsync(user));

            var result = await _userManager.AddToRoleAsync(user, role.Name);
            if (result.Succeeded) return RedirectToAction("UserManagement", _userManager.Users);

            return View(addRoleToUserViewModel);
        }
    }
}